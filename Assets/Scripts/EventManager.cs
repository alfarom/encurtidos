﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

    private static EventManager _instance;

    public static EventManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public float m_eventTimeSeconds;
    private float countTime;
    private int countEvents = 0;
    private int posibilidad = 0;
    public GameObject[] manos;

    public HandController handController;
    public HandController hand1Controller;
    public HandController hand2Controller;
    public HandController hand3Controller;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update () {
        countTime += Time.deltaTime;

        if (countTime > m_eventTimeSeconds)
        {
            //Evaluamos el porcentaje de posibilidades que hay de lanzar cada evento
            if (countEvents < 5)
            {
                posibilidad = Random.Range(1, 6);
            }
            else if ((countEvents >= 5) && (countEvents < 10))
            {
                posibilidad = Random.Range(1, 10);
            }
            else if ((countEvents >= 10) && (countEvents < 15))
            {
                posibilidad = Random.Range(1, 13);
            }
            else if ((countEvents >= 15) && (countEvents < 20))
            {
                posibilidad = Random.Range(1, 15);
            }
            else if (countEvents >= 20)
            {
                posibilidad = Random.Range(1, 16);
            }

            posibilidad = 1;

            if (posibilidad < 6)
            {
                LaunchEventToothpick();
            }
            else if ((posibilidad >= 6) && (posibilidad < 10))
            {
                LaunchEventFork();
            }
            else if ((posibilidad >= 10) && (posibilidad < 13))
            {
                LaunchEventFingers();
            }
            else if ((posibilidad >= 13) && (posibilidad < 15))
            {
                LaunchEventHand();
            }
            else if (posibilidad == 15)
            {
                LaunchEventTongue();
            }

            //Reducimos el intervalo de tiempo cuando cambia de objeto
            if ((countEvents == 5) || (countEvents == 10) || (countEvents == 15) || (countEvents == 20))
            {
                m_eventTimeSeconds = Mathf.Max(1, m_eventTimeSeconds - 1);
            }

            countTime = 0; //Reseteamos el contador de tiempo
            countEvents += 1; //Añadimos 1 al contador de eventos
            posibilidad = 0; //Reseteamos la posibilidad de los eventos
        }
	}

    void LaunchEventToothpick()
    {
        int  manoElegida = Random.Range(0, 4);
        MoveHand(manoElegida);
    }

    void LaunchEventFork()
    {
        print("Evento tenedor");
    }

    void LaunchEventFingers()
    {
        print("Evento dedos");
    }

    void LaunchEventHand()
    {
        print("Evento mano");
    }

    void LaunchEventTongue()
    {
        print("Evento lengua");
    }

    void MoveHand(int manoElegida)
    {
        HandController handController = manos[manoElegida].GetComponent<HandController>();

        if (handController != null)
            if (!handController.flagMove)
            {
                {
                handController.SetFinalPoint();
            }
        }
    }

    public void DesactiveHandColliders()
    {
        foreach(GameObject hand in manos)
        {
            hand.GetComponent<HandController>().DesactiveCollider();
        }
    }
}
