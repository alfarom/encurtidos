﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {

    [SerializeField]
    private Animator m_animator;

    public float Horizontal
    {
        get { return m_animator.GetFloat("Horizontal"); }
        set {
            if(m_animator!=null)
                m_animator.SetFloat("Horizontal", value); }
    }

    public float Vertical
    {
        get { return m_animator.GetFloat("Vertical"); }
        set {
            if (m_animator != null)
                m_animator.SetFloat("Vertical", value); }
    }



    // Use this for initialization
    void Start () {
        m_animator = GetComponentInChildren<Animator>();
    }

}
