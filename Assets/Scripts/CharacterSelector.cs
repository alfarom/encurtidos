﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CharacterSelector : MonoBehaviour {

    public Sprite image;
    public string player;
    public SelectorManager selectorManager;

    // Use this for initialization
    void Start () {
        if (image != null)
            GetComponent<Image>().sprite = image;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetNextCharacter()
    {

    }

    public void GetPreviousCharacter()
    {

    }
}
