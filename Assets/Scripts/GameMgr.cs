﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameMgr : MonoBehaviour {

    private static GameMgr _instance;

    public static GameMgr Instance
    {
        get {return _instance; }
    }

    public List<string> players = new List<string>();

    public GameObject aceitunaPimiento;
    public GameObject aceitunaQueso;
    public GameObject pepinillo;
    public GameObject cebolleta;

    public GameObject youWinPanel;
    public Text winner;
    public GameObject scoreP1;
    public GameObject scoreP2;
    public GameObject scoreP3;
    public GameObject scoreP4;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        InitialitePlayers(PlayerPrefs.GetInt("NumPlayers"));
        AddScoresToHUD();
        Cursor.visible = false;
    }

    public void DeletePlayer(string playerTag)
    {
        players.Remove(playerTag);
        if (players.Count == 1)
        {

            //Destruimos los objetos del juego.
            Destroy(EventManager.Instance);
            //Destroy(GameObject.FindGameObjectWithTag(players[0]));   
            GameObject.FindGameObjectWithTag(players[0]).GetComponent<PlayerMovement>().enabled = false;

            //TODO Show fin del juego.
           
        }
    }

    public void ActiveWinnerPanel()
    {
        Cursor.visible = true;
        youWinPanel.SetActive(true);
        winner.text = players[0];
        AddPlayerScores(players[0]);
        Destroy(GameObject.FindGameObjectWithTag(players[0]));
    }

    public void InitialitePlayers(int numPlayers)
    {
        players.Clear();

        for (int i = 0; i < numPlayers; i++)
        {
            players.Add("Player" + (i+1));
            InstantiatePlayer(players[i]);
        }
    }

    public void InstantiatePlayer(string player)
    {

        if (player.Equals("Player1"))
        {
            Instantiate(aceitunaPimiento);
            scoreP1.SetActive(true);
        }
        else if (player.Equals("Player2"))
        {
            Instantiate(aceitunaQueso);
            scoreP2.SetActive(true);
        }
        else if (player.Equals("Player3"))
        {
            Instantiate(pepinillo);
            scoreP3.SetActive(true);
        }
        else if (player.Equals("Player4"))
        {
            Instantiate(cebolleta);
            scoreP4.SetActive(true);
        }
    }

    public void ResetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void GoToMenu()
    {
        ResetScores();
        SceneManager.LoadScene("Menu");
    }

    public void AddPlayerScores(string player)
    {

        if (player.Equals("Player1"))
        {
            int score = PlayerPrefs.GetInt("ScoreP1");
            PlayerPrefs.SetInt("ScoreP1", ++score);
        }
        else if (player.Equals("Player2"))
        {
            int score = PlayerPrefs.GetInt("ScoreP2");
            PlayerPrefs.SetInt("ScoreP2", ++score);
        }
        else if (player.Equals("Player3"))
        {
            int score = PlayerPrefs.GetInt("ScoreP3");
            PlayerPrefs.SetInt("ScoreP3", ++score);
        }
        else if (player.Equals("Player4"))
        {
            int score = PlayerPrefs.GetInt("ScoreP4");
            PlayerPrefs.SetInt("ScoreP4", ++score);
        }
    }

    public void AddScoresToHUD()
    {
        if (PlayerPrefs.HasKey("ScoreP1"))
            scoreP1.transform.FindChild("Score").GetComponent<Text>().text = PlayerPrefs.GetInt("ScoreP1").ToString();
        if (PlayerPrefs.HasKey("ScoreP2"))
            scoreP2.transform.FindChild("Score").GetComponent<Text>().text = PlayerPrefs.GetInt("ScoreP2").ToString();
        if (PlayerPrefs.HasKey("ScoreP3"))
            scoreP3.transform.FindChild("Score").GetComponent<Text>().text = PlayerPrefs.GetInt("ScoreP3").ToString();
        if (PlayerPrefs.HasKey("ScoreP4"))
            scoreP4.transform.FindChild("Score").GetComponent<Text>().text = PlayerPrefs.GetInt("ScoreP4").ToString();
    }

    public void ResetScores()
    {
        if(PlayerPrefs.HasKey("ScoreP1"))
            PlayerPrefs.SetInt("ScoreP1", 0);
        if (PlayerPrefs.HasKey("ScoreP2"))
            PlayerPrefs.SetInt("ScoreP2", 0);
        if (PlayerPrefs.HasKey("ScoreP3"))
            PlayerPrefs.SetInt("ScoreP3", 0);
        if (PlayerPrefs.HasKey("ScoreP4"))
            PlayerPrefs.SetInt("ScoreP4", 0);
    }
}
