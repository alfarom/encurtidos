﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerTagsFade : MonoBehaviour {

    public float timer=0.0f;
    public float timeToFade=2.0f;
    private bool faded=false;
    public SpriteRenderer image;
    public Color32 targetColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);

    private void Start()
    {
        image = GetComponent<SpriteRenderer>();
    }

    void Update() {
        if (!faded) { 
            timer += Time.deltaTime;
            if (timer > timeToFade)
            {
                faded = true;
            }
        }
        else
        {
            image.color = Color32.Lerp(image.color, targetColor, 0.1f);
            if (image.color.Equals(targetColor))
            {
                Destroy(gameObject);
            }
        }
	}





}
