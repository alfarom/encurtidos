﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandController : MonoBehaviour {

    private Vector3 finalPoint = new Vector3();
    private Vector3 initialPoint = new Vector3();
    private Vector3 handPosition = new Vector3();
    public bool flagMove;
    public float m_Velocity;
    private float velocity;
    private float distanceInicial;
    private float valX;
    private float valY;
    private float distancef1;
    private float distancef2;
    private Animator handAnimator;
    private bool isColliderActive;
    private GameObject playerToDestroy;
    public Transform palilloTransform;
    public GameObject shadow;
    private GameObject instantiateShadow;

    public AudioClip toothpickHit_0;
    public AudioClip toothpickHit_1;
    public AudioClip toothpickHit_2;

    public void SetFinalPoint()
    {
        //Calculamos la distacia a los dos puntos de foco de la elipse
        //valX = Random.Range(-12, 12);
        //valY = Mathf.Sqrt(144 - Mathf.Pow(valX, 2)) / 3;
        
        ////Primero controlamos que la mano esté fuera de la pantalla
        //handPosition = gameObject.transform.FindChild("Sprite").position;
        
        //if ((handPosition.x > 29) || (handPosition.x < -29) || (handPosition.y > 10))
        //{
        //    finalPoint = new Vector3(((valX - 1) * 9 / 11),
        //        Random.Range(- ((valY + 1) * 9 / 11), ((valY - 1) * 9 / 11)), 0);

          
        //}

        int random = Random.Range(0, GameMgr.Instance.players.Count);
        GameObject player = GameObject.FindGameObjectWithTag(GameMgr.Instance.players[random]);
        finalPoint = player.transform.position;

        instantiateShadow = Instantiate(shadow, player.transform.position - (Vector3.up * player.GetComponent<CapsuleCollider2D>().bounds.extents.y), Quaternion.identity);

        flagMove = true;
    }

    void Start()
    {
        initialPoint = transform.position;
        handAnimator = gameObject.transform.FindChild("Sprite").GetComponent<Animator>();
    }
    
    // Update is called once per frame
	void Update () {
        velocity = m_Velocity;
        
        if (flagMove)
        {
            float distance = Vector3.Distance(finalPoint, transform.position);

            if (distance < 0.2f && !isColliderActive)
            {
                if (initialPoint == finalPoint)
                {
                    if (playerToDestroy != null)
                    {
                        Destroy(playerToDestroy);
                        playerToDestroy = null;
                        if (GameMgr.Instance.players.Count == 1)
                        {
                            GameMgr.Instance.ActiveWinnerPanel();
                        }
                    }
                    flagMove = false;
                }
                handAnimator.SetTrigger("isPicking");
                transform.FindChild("Collider").gameObject.SetActive(true);
                isColliderActive = true;  
            }

            if (!handAnimator.GetCurrentAnimatorStateInfo(0).IsName("Pick"))
            {
                Move(velocity, finalPoint);
            }
        }
	}

    void Move(float velocity, Vector3 m_Target)
    {
        transform.position = Vector3.Lerp(transform.position, m_Target, velocity * 0.2f * Time.deltaTime);
        //print("la nueva posicion: " + transform.position);
    }

    public void DesactiveCollider()
    {
        transform.FindChild("Collider").gameObject.SetActive(false);
        finalPoint = initialPoint;
        isColliderActive = false;
    }

    public void SetPlayerToDestroy(GameObject player)
    {
        playerToDestroy = player;
    }

    public void SetPlayerToParent()
    {
        if (playerToDestroy != null)
        {
            playerToDestroy.transform.parent = palilloTransform;
            playerToDestroy.transform.localPosition = Vector3.zero;
            playerToDestroy.transform.localScale = Vector3.one;
            playerToDestroy.gameObject.transform.FindChild("Animator").GetComponent<Renderer>().sortingOrder = 30;
        }
        Destroy(instantiateShadow);
        instantiateShadow = null;
    }

}
