﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    [SerializeField]
    private float m_speed;

    [SerializeField]
    private float m_rotateSpeed;

    [SerializeField]
    private float m_impulseForce;

    private Rigidbody2D m_rigigbody;
    private AnimationController animationController;

    private string horizontal;
    private string vertical;
    private bool isImpulse;

    // Use this for initialization
    void Start()
    {
        m_rigigbody = GetComponent<Rigidbody2D>();
        animationController = GetComponent<AnimationController>();
        horizontal = PlayerInputs.GetInputHorizontal(tag);
        vertical = PlayerInputs.GetInputVertical(tag);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isImpulse) { 
            m_rigigbody.velocity = Vector3.zero;
            Vector3 movement = Vector3.zero;
            if (Input.GetAxis(horizontal) > 0.25 || Input.GetAxis(horizontal) < -0.25 || Input.GetAxis(vertical) > 0.25 || Input.GetAxis(vertical) < -0.25)
                movement = new Vector3(Input.GetAxis(horizontal) * m_speed, Input.GetAxis(vertical) * m_speed, 0) * Time.deltaTime;
            transform.Translate(movement);
            UpdateAnimator();
        }
    }

    void UpdateAnimator()
    {
        print(Input.GetAxis(horizontal));
        if (Input.GetAxis(horizontal) > 0.25 || Input.GetAxis(horizontal) < -0.25)
            animationController.Horizontal = Input.GetAxis(horizontal);
        else
            animationController.Horizontal = 0;

        if (Input.GetAxis(vertical) > 0.25 || Input.GetAxis(vertical) < -0.25)
            animationController.Vertical = Input.GetAxis(vertical);
        else
            animationController.Vertical = 0;

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Rigidbody2D>() != null)
        {
            Vector3 impulseVector = collision.transform.position - transform.position ;
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(impulseVector * m_speed * m_impulseForce, ForceMode2D.Impulse);
            collision.gameObject.GetComponent<PlayerMovement>().StartCoroutine("ImpulseOlive");
        }
    }

    public IEnumerator ImpulseOlive()
    {
        isImpulse = true;
        yield return new WaitForSeconds(0.5f);
        isImpulse = false;
    }

}
