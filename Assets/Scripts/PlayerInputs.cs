﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputs  {

    public static readonly string PLAYER_1_HORIZONTAL = "Player1Horizontal";
    public static readonly string PLAYER_2_HORIZONTAL = "Player2Horizontal";
    public static readonly string PLAYER_3_HORIZONTAL = "Player3Horizontal";
    public static readonly string PLAYER_4_HORIZONTAL = "Player4Horizontal";

    public static readonly string PLAYER_1_VERTICAL = "Player1Vertical";
    public static readonly string PLAYER_2_VERTICAL = "Player2Vertical";
    public static readonly string PLAYER_3_VERTICAL = "Player3Vertical";
    public static readonly string PLAYER_4_VERTICAL = "Player4Vertical";

    public static string GetInputHorizontal(string tag)
    {
        if (tag.Equals("Player1"))
        {
            return PLAYER_1_HORIZONTAL;
        }
        else if (tag.Equals("Player2"))
        {
            return PLAYER_2_HORIZONTAL;
        }
        else if (tag.Equals("Player3"))
        {
            return PLAYER_3_HORIZONTAL;
        }
        else if (tag.Equals("Player4"))
        {
            return PLAYER_4_HORIZONTAL;
        }

        return null;
    }

    public static string GetInputVertical(string tag)
    {
        if (tag.Equals("Player1"))
        {
            return PLAYER_1_VERTICAL;
        }
        else if (tag.Equals("Player2"))
        {
            return PLAYER_2_VERTICAL;
        }
        else if (tag.Equals("Player3"))
        {
            return PLAYER_3_VERTICAL;
        }
        else if (tag.Equals("Player4"))
        {
            return PLAYER_4_VERTICAL;
        }

        return null;
    }
}
