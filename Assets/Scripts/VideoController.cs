﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VideoController : MonoBehaviour {
    
    private void Awake()
    {
        ((MovieTexture)GetComponent<Renderer>().material.mainTexture).Play();
    }
    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update()
    {
        if (!((MovieTexture)GetComponent<Renderer>().material.mainTexture).isPlaying)
        {
            SceneManager.LoadScene("Menu");
        }
        if (Input.GetButtonDown("Jump")) // Skip with space
        {

            Renderer r = GetComponent<Renderer>();
            MovieTexture movie = (MovieTexture)r.material.mainTexture;
            movie.Stop();
        }
    }
}
