﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class SelectorManager : MonoBehaviour {

    public CharacterSelector[] playerCharacters;
    private int numPlayers;
    public Text numPlayersText;
    public GameObject pepinilloMenu;
    public GameObject cebolletaMenu;


    // Use this for initialization
    void Start () {
        numPlayers = 2;
        AddMenus();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            //TODO hacer visible player 3 si no esta visible
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            //TODO hacer visible player 4 si no esta visible
        }
    }


    public void Play()
    {
        ResetScores();
        SceneManager.LoadScene("Scene1");
    }

    public void Back()
    {
        SceneManager.LoadScene("Menu");
    }

    public void AddNumPlayers()
    {
        if (++numPlayers > 4)
        {
            numPlayers = 4;
        }
        SetNumPlayersText(numPlayers);
        AddMenus();

    }

    public void DeleteNumPlayers()
    {
        if (--numPlayers <2)
        {
            numPlayers = 2;
        }
        SetNumPlayersText(numPlayers);
        AddMenus();
    }

    public void AddMenus()
    {
        if (numPlayers == 2)
        {
            pepinilloMenu.SetActive(false);
            cebolletaMenu.SetActive(false);
        }
        else if (numPlayers == 3)
        {
            pepinilloMenu.SetActive(true);
            cebolletaMenu.SetActive(false);
        }
        else if (numPlayers == 4)
        {
            pepinilloMenu.SetActive(true);
            cebolletaMenu.SetActive(true);
        }

        PlayerPrefs.SetInt("NumPlayers", numPlayers);
    }

    public void SetNumPlayersText(int num)
    {
        numPlayersText.text = num.ToString();
    }

    public void ResetScores()
    {
        if (PlayerPrefs.HasKey("ScoreP1"))
            PlayerPrefs.SetInt("ScoreP1", 0);
        if (PlayerPrefs.HasKey("ScoreP2"))
            PlayerPrefs.SetInt("ScoreP2", 0);
        if (PlayerPrefs.HasKey("ScoreP3"))
            PlayerPrefs.SetInt("ScoreP3", 0);
        if (PlayerPrefs.HasKey("ScoreP4"))
            PlayerPrefs.SetInt("ScoreP4", 0);
    }
}

[System.Serializable]
public class PlayerCharacter
{

    public Sprite image;
    public string player;

}
