﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public GameObject gamePanel;
    public GameObject controlsPanel;


    public void clickControls()
    {
        gamePanel.SetActive(false);
        controlsPanel.SetActive(true);
    }

    public void clickBack()
    {
        controlsPanel.SetActive(false);
        gamePanel.SetActive(true);
    }

    public void clickPlay()
    {
        SceneManager.LoadScene("CharacterSelectionScene");
    }

    public void clickExit()
    {
        Application.Quit();
    }
}
