﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderHandHandler : MonoBehaviour {

    private GameObject playerToDestroy;

    void OnTriggerEnter2D(Collider2D other)
    {
        print("Choca?");
        if (playerToDestroy != null) return;

        bool found = false;

        if (other.gameObject.tag == "Player1")
        {
            found = true;
        }
        else if (other.gameObject.tag == "Player2")
        {
            found = true;
        }
        else if (other.gameObject.tag == "Player3")
        {
            found = true;
        }
        else if (other.gameObject.tag == "Player4")
        {
            found = true;
        } else if (other.gameObject.tag == "Fondo")
        {
            switch (Random.Range(0, 3))
            {
                case 0:
                    transform.parent.GetComponent<AudioSource>().clip = transform.parent.GetComponent<HandController>().toothpickHit_0;
                    break;
                case 1:
                    transform.parent.GetComponent<AudioSource>().clip = transform.parent.GetComponent<HandController>().toothpickHit_1;
                    break;
                case 2:
                    transform.parent.GetComponent<AudioSource>().clip = transform.parent.GetComponent<HandController>().toothpickHit_2;
                    break;
            }
            transform.parent.GetComponent<AudioSource>().Play();
            print("Entra??");
        }


        if (found)
        {
            playerToDestroy = other.gameObject;
            if (GameMgr.Instance.players.Count > 1)
            {
                GameMgr.Instance.DeletePlayer(playerToDestroy.tag); 
            }
            other.transform.parent = transform.parent;
            //other.transform.localPosition = Vector3.right * -2;
            other.transform.localPosition = Vector3.zero;
            other.GetComponent<PlayerMovement>().enabled = false;
            other.GetComponent<CapsuleCollider2D>().enabled = false;
            other.SendMessage("PlayDeathSound");
            SendMessageUpwards("SetPlayerToDestroy", playerToDestroy);
            gameObject.SetActive(false);
        }
    }
}
