﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudioMgr : MonoBehaviour {

    public AudioSource audioSource;

    public AudioClip deathSound;

    public void PlayDeathSound()
    {
        audioSource.PlayOneShot(deathSound);
    }
        
}
